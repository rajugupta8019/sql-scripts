USE [oline]
GO
/****** Object:  Table [dbo].[oline_software]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oline_software](
	[originalFileName] [varchar](255) NOT NULL,
	[productCode] [varchar](100) NOT NULL,
 CONSTRAINT [PK_oline_software] PRIMARY KEY CLUSTERED 
(
	[originalFileName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
