USE [oline]
GO
/****** Object:  Table [dbo].[oline_reportConfig]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oline_reportConfig](
	[reportKey] [varchar](20) NOT NULL,
	[configKey] [varchar](50) NOT NULL,
	[configValue] [varchar](1000) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
