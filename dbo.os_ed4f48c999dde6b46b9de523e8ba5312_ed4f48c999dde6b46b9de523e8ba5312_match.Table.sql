USE [oline]
GO
/****** Object:  Table [dbo].[os_ed4f48c999dde6b46b9de523e8ba5312_ed4f48c999dde6b46b9de523e8ba5312_match]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_ed4f48c999dde6b46b9de523e8ba5312_ed4f48c999dde6b46b9de523e8ba5312_match](
	[personId] [datetime2](3) NOT NULL,
	[matchPersonId] [datetime2](3) NOT NULL,
	[matchScore] [smallint] NOT NULL,
	[matchType] [tinyint] NOT NULL
) ON [PRIMARY]

GO
