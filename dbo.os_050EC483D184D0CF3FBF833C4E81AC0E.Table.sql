USE [oline]
GO
/****** Object:  Table [dbo].[os_050EC483D184D0CF3FBF833C4E81AC0E]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_050EC483D184D0CF3FBF833C4E81AC0E](
	[personId] [datetime2](3) NOT NULL,
	[fileName] [nvarchar](100) NOT NULL,
	[matchFileName] [nvarchar](100) NULL,
	[score] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_score]  DEFAULT ((0)),
	[isMerged] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_isMerged]  DEFAULT ((0)),
	[timestamp] [datetime2](3) NOT NULL,
	[gender] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_gender]  DEFAULT ((0)),
	[age] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_age]  DEFAULT ((0)),
	[maleScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_maleScoreNormalized]  DEFAULT ((0)),
	[femaleScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_femaleScoreNormalized]  DEFAULT ((0)),
	[childScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_childScoreNormalized]  DEFAULT ((0)),
	[teenScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_teenScoreNormalized]  DEFAULT ((0)),
	[youngAdultScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_youngAdultScoreNormalized]  DEFAULT ((0)),
	[adultScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_adultScoreNormalized]  DEFAULT ((0)),
	[middleAgedScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_middleAgedScoreNormalized]  DEFAULT ((0)),
	[seniorScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_seniorScoreNormalized]  DEFAULT ((0)),
	[trackId] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_trackId]  DEFAULT ((0)),
	[normalizedGender] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_normalizedGender]  DEFAULT ((0)),
	[normalizedAge] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_normalizedAge]  DEFAULT ((0)),
	[normalizedAgeString] [nvarchar](50) NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_normalizedAgeString]  DEFAULT (N'N/A'),
	[maxThumbnailIndex] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_maxThumbnailIndex]  DEFAULT ((0)),
	[id] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_id]  DEFAULT ((0)),
	[maleScore] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_maleScore]  DEFAULT ((0)),
	[femaleScore] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_femaleScore]  DEFAULT ((0)),
	[enrolled] [smallint] NOT NULL CONSTRAINT [DF_os_050EC483D184D0CF3FBF833C4E81AC0E_enrolled]  DEFAULT ((0))
) ON [PRIMARY]

GO
