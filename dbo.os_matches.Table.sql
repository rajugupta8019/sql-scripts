USE [oline]
GO
/****** Object:  Table [dbo].[os_matches]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_matches](
	[personId] [nvarchar](50) NOT NULL,
	[matchPersonId] [nvarchar](50) NOT NULL,
	[matchScore] [smallint] NOT NULL,
	[matchType] [smallint] NOT NULL
) ON [PRIMARY]

GO
