USE [oline]
GO
/****** Object:  Table [dbo].[os_F2129581FD6211F9D11FEEB429AA4011_match]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_F2129581FD6211F9D11FEEB429AA4011_match](
	[personId] [datetime2](3) NOT NULL,
	[matchPersonId] [datetime2](3) NOT NULL,
	[matchScore] [smallint] NOT NULL,
	[matchType] [tinyint] NOT NULL
) ON [PRIMARY]

GO
