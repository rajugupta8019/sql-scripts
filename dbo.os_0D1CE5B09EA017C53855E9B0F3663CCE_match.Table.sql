USE [oline]
GO
/****** Object:  Table [dbo].[os_0D1CE5B09EA017C53855E9B0F3663CCE_match]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_0D1CE5B09EA017C53855E9B0F3663CCE_match](
	[personId] [datetime2](3) NOT NULL,
	[matchPersonId] [datetime2](3) NOT NULL,
	[matchScore] [smallint] NOT NULL,
	[matchType] [tinyint] NOT NULL
) ON [PRIMARY]

GO
