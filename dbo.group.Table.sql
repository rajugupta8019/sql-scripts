USE [oline]
GO
/****** Object:  Table [dbo].[group]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[group](
	[groupID] [int] NOT NULL,
	[groupName] [nvarchar](100) NOT NULL,
	[inchargeID] [int] NOT NULL,
	[timezone] [varchar](15) NULL,
	[password] [varchar](256) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
