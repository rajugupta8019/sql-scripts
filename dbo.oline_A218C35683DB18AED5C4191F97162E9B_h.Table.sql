USE [oline]
GO
/****** Object:  Table [dbo].[oline_A218C35683DB18AED5C4191F97162E9B_h]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oline_A218C35683DB18AED5C4191F97162E9B_h](
	[dateInfo] [datetime] NOT NULL,
	[entryCount] [int] NULL,
	[exitCount] [int] NULL,
 CONSTRAINT [PK_oline_A218C35683DB18AED5C4191F97162E9B_h] PRIMARY KEY CLUSTERED 
(
	[dateInfo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
