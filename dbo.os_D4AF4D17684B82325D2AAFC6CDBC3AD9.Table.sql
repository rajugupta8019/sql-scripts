USE [oline]
GO
/****** Object:  Table [dbo].[os_D4AF4D17684B82325D2AAFC6CDBC3AD9]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_D4AF4D17684B82325D2AAFC6CDBC3AD9](
	[personId] [datetime2](3) NOT NULL,
	[fileName] [nvarchar](100) NOT NULL,
	[matchFileName] [nvarchar](100) NULL,
	[score] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_score]  DEFAULT ((0)),
	[isMerged] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_isMerged]  DEFAULT ((0)),
	[timestamp] [datetime2](3) NOT NULL,
	[gender] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_gender]  DEFAULT ((0)),
	[age] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_age]  DEFAULT ((0)),
	[maleScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_maleScoreNormalized]  DEFAULT ((0)),
	[femaleScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_femaleScoreNormalized]  DEFAULT ((0)),
	[childScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_childScoreNormalized]  DEFAULT ((0)),
	[teenScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_teenScoreNormalized]  DEFAULT ((0)),
	[youngAdultScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_youngAdultScoreNormalized]  DEFAULT ((0)),
	[adultScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_adultScoreNormalized]  DEFAULT ((0)),
	[middleAgedScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_middleAgedScoreNormalized]  DEFAULT ((0)),
	[seniorScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_seniorScoreNormalized]  DEFAULT ((0)),
	[trackId] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_trackId]  DEFAULT ((0)),
	[normalizedGender] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_normalizedGender]  DEFAULT ((0)),
	[normalizedAge] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_normalizedAge]  DEFAULT ((0)),
	[normalizedAgeString] [nvarchar](50) NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_normalizedAgeString]  DEFAULT (N'N/A'),
	[maxThumbnailIndex] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_maxThumbnailIndex]  DEFAULT ((0)),
	[id] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_id]  DEFAULT ((0)),
	[maleScore] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_maleScore]  DEFAULT ((0)),
	[femaleScore] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_femaleScore]  DEFAULT ((0)),
	[enrolled] [smallint] NOT NULL CONSTRAINT [DF_os_D4AF4D17684B82325D2AAFC6CDBC3AD9_enrolled]  DEFAULT ((0))
) ON [PRIMARY]

GO
