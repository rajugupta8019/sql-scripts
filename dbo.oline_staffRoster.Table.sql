USE [oline]
GO
/****** Object:  Table [dbo].[oline_staffRoster]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oline_staffRoster](
	[staffId] [varchar](50) NOT NULL,
	[rosteredDate] [datetime2](3) NOT NULL,
	[locationId] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
