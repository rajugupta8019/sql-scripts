USE [oline]
GO
/****** Object:  Table [dbo].[oline_reportAction]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oline_reportAction](
	[reportActionId] [int] IDENTITY(1,1) NOT NULL,
	[actionBy] [varchar](10) NOT NULL,
	[actionType] [varchar](10) NOT NULL,
	[actionFor] [varchar](255) NOT NULL,
	[remark] [varchar](255) NOT NULL,
	[reportKey] [varchar](50) NOT NULL,
 CONSTRAINT [PK_oline_reportAction] PRIMARY KEY CLUSTERED 
(
	[reportActionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
