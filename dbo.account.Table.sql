USE [oline]
GO
/****** Object:  Table [dbo].[account]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[account](
	[userID] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](100) NOT NULL,
	[password] [varchar](256) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[contact] [nvarchar](50) NOT NULL CONSTRAINT [DF_table_new3_contact]  DEFAULT ((88888888)),
	[securityToken] [varchar](256) NULL,
	[lastLogin] [datetime] NULL,
	[groupID] [int] NOT NULL,
	[privilege] [varchar](50) NOT NULL CONSTRAINT [DF_table_new3_privilege]  DEFAULT ('Normal'),
	[threshold] [int] NOT NULL CONSTRAINT [DF_table_new3_threshold]  DEFAULT ((3)),
	[defaultView] [varchar](50) NOT NULL CONSTRAINT [DF_table_new3_defaultView]  DEFAULT ('Entry'),
	[startTime] [char](10) NOT NULL CONSTRAINT [DF_table_new3_startTime]  DEFAULT ((830)),
	[closeTime] [char](10) NOT NULL CONSTRAINT [DF_table_new3_closeTime]  DEFAULT ((2130)),
	[reportType] [varchar](50) NULL,
	[refreshInterval] [nvarchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
