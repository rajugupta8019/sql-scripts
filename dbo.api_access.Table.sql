USE [oline]
GO
/****** Object:  Table [dbo].[api_access]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[api_access](
	[apiId] [int] NOT NULL,
	[userGroupId] [int] NOT NULL,
	[accessId] [int] NOT NULL
) ON [PRIMARY]

GO
