USE [oline]
GO
/****** Object:  Table [dbo].[oline_e8757f000a4eB_h]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oline_e8757f000a4eB_h](
	[dateInfo] [datetime] NOT NULL,
	[entryCount] [int] NULL,
	[exitCount] [int] NULL,
 CONSTRAINT [PK_oline_e8757f000a4e_h] PRIMARY KEY CLUSTERED 
(
	[dateInfo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
