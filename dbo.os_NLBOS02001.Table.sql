USE [oline]
GO
/****** Object:  Table [dbo].[os_NLBOS02001]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_NLBOS02001](
	[personId] [datetime2](3) NOT NULL,
	[fileName] [nvarchar](100) NOT NULL,
	[matchFileName] [nvarchar](100) NULL,
	[score] [smallint] NOT NULL,
	[isMerged] [smallint] NOT NULL,
	[timestamp] [datetime2](3) NOT NULL,
	[gender] [smallint] NOT NULL,
	[age] [smallint] NOT NULL,
	[maleScoreNormalized] [smallint] NOT NULL,
	[femaleScoreNormalized] [smallint] NOT NULL,
	[childScoreNormalized] [smallint] NOT NULL,
	[teenScoreNormalized] [smallint] NOT NULL,
	[youngAdultScoreNormalized] [smallint] NOT NULL,
	[adultScoreNormalized] [smallint] NOT NULL,
	[middleAgedScoreNormalized] [smallint] NOT NULL,
	[seniorScoreNormalized] [smallint] NOT NULL,
	[trackId] [smallint] NOT NULL,
	[normalizedGender] [smallint] NOT NULL,
	[normalizedAge] [smallint] NOT NULL,
	[normalizedAgeString] [nvarchar](50) NOT NULL,
	[maxThumbnailIndex] [smallint] NOT NULL,
	[id] [smallint] NOT NULL,
	[maleScore] [smallint] NOT NULL,
	[femaleScore] [smallint] NOT NULL,
	[enrolled] [smallint] NOT NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_score]  DEFAULT ((0)) FOR [score]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_isMerged]  DEFAULT ((0)) FOR [isMerged]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_gender]  DEFAULT ((0)) FOR [gender]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_age]  DEFAULT ((0)) FOR [age]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_maleScoreNormalized]  DEFAULT ((0)) FOR [maleScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_femaleScoreNormalized]  DEFAULT ((0)) FOR [femaleScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_childScoreNormalized]  DEFAULT ((0)) FOR [childScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_teenScoreNormalized]  DEFAULT ((0)) FOR [teenScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_youngAdultScoreNormalized]  DEFAULT ((0)) FOR [youngAdultScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_adultScoreNormalized]  DEFAULT ((0)) FOR [adultScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_middleAgedScoreNormalized]  DEFAULT ((0)) FOR [middleAgedScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_seniorScoreNormalized]  DEFAULT ((0)) FOR [seniorScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_trackId]  DEFAULT ((0)) FOR [trackId]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_normalizedGender]  DEFAULT ((0)) FOR [normalizedGender]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_normalizedAge]  DEFAULT ((0)) FOR [normalizedAge]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_normalizedAgeString]  DEFAULT (N'N/A') FOR [normalizedAgeString]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_maxThumbnailIndex]  DEFAULT ((0)) FOR [maxThumbnailIndex]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_id]  DEFAULT ((0)) FOR [id]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_maleScore]  DEFAULT ((0)) FOR [maleScore]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_femaleScore]  DEFAULT ((0)) FOR [femaleScore]
GO
ALTER TABLE [dbo].[os_NLBOS02001] ADD  CONSTRAINT [DF_os_NLBOS02001_enrolled]  DEFAULT ((0)) FOR [enrolled]
GO
