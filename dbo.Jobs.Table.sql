USE [oline]
GO
/****** Object:  Table [dbo].[Jobs]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Jobs](
	[JobId] [nvarchar](50) NOT NULL,
	[JobDescription] [nvarchar](max) NULL,
	[ServiceId] [nvarchar](50) NOT NULL,
	[AssemblyName] [varchar](50) NOT NULL,
	[ClassName] [varchar](50) NOT NULL,
	[LogFolder] [varchar](max) NOT NULL,
	[LogLevel] [int] NOT NULL,
	[ScheduleType] [varchar](50) NULL,
	[ScheduleDefinition] [varchar](50) NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_Jobs_Active]  DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
