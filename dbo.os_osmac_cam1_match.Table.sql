USE [oline]
GO
/****** Object:  Table [dbo].[os_osmac_cam1_match]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_osmac_cam1_match](
	[personId] [datetime] NOT NULL,
	[matchPersonId] [datetime] NOT NULL,
	[matchScore] [smallint] NOT NULL,
	[matchType] [tinyint] NOT NULL
) ON [PRIMARY]

GO
