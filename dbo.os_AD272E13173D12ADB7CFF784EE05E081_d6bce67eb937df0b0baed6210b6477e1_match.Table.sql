USE [oline]
GO
/****** Object:  Table [dbo].[os_AD272E13173D12ADB7CFF784EE05E081_d6bce67eb937df0b0baed6210b6477e1_match]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_AD272E13173D12ADB7CFF784EE05E081_d6bce67eb937df0b0baed6210b6477e1_match](
	[personId] [datetime2](3) NOT NULL,
	[matchPersonId] [datetime2](3) NOT NULL,
	[matchScore] [smallint] NOT NULL,
	[matchType] [tinyint] NOT NULL
) ON [PRIMARY]

GO
