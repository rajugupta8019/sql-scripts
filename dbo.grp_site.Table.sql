USE [oline]
GO
/****** Object:  Table [dbo].[grp_site]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[grp_site](
	[groupID] [int] NOT NULL,
	[siteID] [int] NOT NULL,
	[dateInfo] [datetime] NOT NULL,
	[weekOfMonth] [int] NULL,
	[trafficFlow] [int] NULL,
	[revenue] [int] NULL,
	[totalTransaction] [int] NULL,
	[trafficFlowExit] [int] NULL,
	[conversionRatio] [decimal](9, 2) NULL,
	[DollarPerShopper] [decimal](9, 2) NULL
) ON [PRIMARY]

GO
