USE [oline]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetJobs]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Raju Gupta>
-- Description:	<For checking the job>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetJobs]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;  
	SELECT * FROM Jobs where Active = 1
END
GO
