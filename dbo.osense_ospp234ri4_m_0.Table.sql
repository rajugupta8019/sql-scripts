USE [oline]
GO
/****** Object:  Table [dbo].[osense_ospp234ri4_m_0]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[osense_ospp234ri4_m_0](
	[dateInfo] [date] NOT NULL,
	[male] [int] NULL,
	[female] [int] NULL,
	[other] [int] NULL,
	[child] [int] NULL,
	[teen] [int] NULL,
	[youngAdult] [int] NULL,
	[adult] [int] NULL,
	[middleAged] [int] NULL,
	[Senior] [int] NULL,
	[mchild] [int] NULL,
	[mteen] [int] NULL,
	[myoungAdult] [int] NULL,
	[madult] [int] NULL,
	[mmiddleAged] [int] NULL,
	[mSenior] [int] NULL,
	[fchild] [int] NULL,
	[fteen] [int] NULL,
	[fyoungAdult] [int] NULL,
	[fadult] [int] NULL,
	[fmiddleAged] [int] NULL,
	[fSenior] [int] NULL,
	[ochild] [int] NULL,
	[oteen] [int] NULL,
	[oyoungAdult] [int] NULL,
	[oadult] [int] NULL,
	[omiddleAged] [int] NULL,
	[oSenior] [int] NULL,
	[lt15] [int] NULL,
	[lt30] [int] NULL,
	[lt60] [int] NULL,
	[lt120] [int] NULL,
	[gt120] [int] NULL
) ON [PRIMARY]

GO
