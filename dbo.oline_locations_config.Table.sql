USE [oline]
GO
/****** Object:  Table [dbo].[oline_locations_config]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oline_locations_config](
	[locationId] [int] NOT NULL,
	[key] [nvarchar](64) NOT NULL,
	[value] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
