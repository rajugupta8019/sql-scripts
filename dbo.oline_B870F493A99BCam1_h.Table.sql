USE [oline]
GO
/****** Object:  Table [dbo].[oline_B870F493A99BCam1_h]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oline_B870F493A99BCam1_h](
	[dateInfo] [datetime] NOT NULL,
	[entryCount] [int] NULL,
	[exitCount] [int] NULL,
 CONSTRAINT [PK_oline_B870F493A99BCam1_h] PRIMARY KEY CLUSTERED 
(
	[dateInfo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
