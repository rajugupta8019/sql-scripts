USE [oline]
GO
/****** Object:  Table [dbo].[oline_license]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oline_license](
	[vendorId] [nvarchar](50) NOT NULL,
	[data] [nvarchar](max) NOT NULL,
	[timestamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
