USE [oline]
GO
/****** Object:  Table [dbo].[oline_devices_databin]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oline_devices_databin](
	[MacAddress] [varchar](32) NOT NULL,
	[Property] [varchar](64) NOT NULL,
	[Value] [varchar](256) NOT NULL,
	[DateInfo] [datetime] NULL,
	[Data] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
