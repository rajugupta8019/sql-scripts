USE [oline]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAccountForReport]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Raju Gupta>
-- Description:	<selecting account for report>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetAccountForReport]	
@ScheduleType varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;  
	select * from account where reportType=@ScheduleType
END
GO
