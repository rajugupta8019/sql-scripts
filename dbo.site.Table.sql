USE [oline]
GO
/****** Object:  Table [dbo].[site]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[site](
	[siteID] [int] NOT NULL,
	[siteName] [varchar](150) NULL,
	[color] [varchar](50) NULL,
	[deviceID] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
