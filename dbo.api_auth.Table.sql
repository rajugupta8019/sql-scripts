USE [oline]
GO
/****** Object:  Table [dbo].[api_auth]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[api_auth](
	[apiKey] [varchar](128) NOT NULL,
	[apiSecret] [varchar](128) NOT NULL,
	[apiId] [int] NOT NULL,
	[apiVendor] [varchar](128) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
