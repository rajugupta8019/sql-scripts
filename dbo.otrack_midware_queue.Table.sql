USE [oline]
GO
/****** Object:  Table [dbo].[otrack_midware_queue]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[otrack_midware_queue](
	[pkey] [bigint] IDENTITY(1,1) NOT NULL,
	[macAddress] [nchar](12) NOT NULL,
	[startDT] [datetime] NOT NULL,
	[endDT] [datetime] NOT NULL,
	[feedback] [nchar](10) NULL
) ON [PRIMARY]

GO
