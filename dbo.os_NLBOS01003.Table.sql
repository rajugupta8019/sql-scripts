USE [oline]
GO
/****** Object:  Table [dbo].[os_NLBOS01003]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_NLBOS01003](
	[personId] [datetime2](3) NOT NULL,
	[fileName] [nvarchar](100) NOT NULL,
	[matchFileName] [nvarchar](100) NULL,
	[score] [smallint] NOT NULL,
	[isMerged] [smallint] NOT NULL,
	[timestamp] [datetime2](3) NOT NULL,
	[gender] [smallint] NOT NULL,
	[age] [smallint] NOT NULL,
	[maleScoreNormalized] [smallint] NOT NULL,
	[femaleScoreNormalized] [smallint] NOT NULL,
	[childScoreNormalized] [smallint] NOT NULL,
	[teenScoreNormalized] [smallint] NOT NULL,
	[youngAdultScoreNormalized] [smallint] NOT NULL,
	[adultScoreNormalized] [smallint] NOT NULL,
	[middleAgedScoreNormalized] [smallint] NOT NULL,
	[seniorScoreNormalized] [smallint] NOT NULL,
	[trackId] [smallint] NOT NULL,
	[normalizedGender] [smallint] NOT NULL,
	[normalizedAge] [smallint] NOT NULL,
	[normalizedAgeString] [nvarchar](50) NOT NULL,
	[maxThumbnailIndex] [smallint] NOT NULL,
	[id] [smallint] NOT NULL,
	[maleScore] [smallint] NOT NULL,
	[femaleScore] [smallint] NOT NULL,
	[enrolled] [smallint] NOT NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_score]  DEFAULT ((0)) FOR [score]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_isMerged]  DEFAULT ((0)) FOR [isMerged]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_gender]  DEFAULT ((0)) FOR [gender]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_age]  DEFAULT ((0)) FOR [age]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_maleScoreNormalized]  DEFAULT ((0)) FOR [maleScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_femaleScoreNormalized]  DEFAULT ((0)) FOR [femaleScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_childScoreNormalized]  DEFAULT ((0)) FOR [childScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_teenScoreNormalized]  DEFAULT ((0)) FOR [teenScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_youngAdultScoreNormalized]  DEFAULT ((0)) FOR [youngAdultScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_adultScoreNormalized]  DEFAULT ((0)) FOR [adultScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_middleAgedScoreNormalized]  DEFAULT ((0)) FOR [middleAgedScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_seniorScoreNormalized]  DEFAULT ((0)) FOR [seniorScoreNormalized]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_trackId]  DEFAULT ((0)) FOR [trackId]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_normalizedGender]  DEFAULT ((0)) FOR [normalizedGender]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_normalizedAge]  DEFAULT ((0)) FOR [normalizedAge]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_normalizedAgeString]  DEFAULT (N'N/A') FOR [normalizedAgeString]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_maxThumbnailIndex]  DEFAULT ((0)) FOR [maxThumbnailIndex]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_id]  DEFAULT ((0)) FOR [id]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_maleScore]  DEFAULT ((0)) FOR [maleScore]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_femaleScore]  DEFAULT ((0)) FOR [femaleScore]
GO
ALTER TABLE [dbo].[os_NLBOS01003] ADD  CONSTRAINT [DF_os_NLBOS01003_enrolled]  DEFAULT ((0)) FOR [enrolled]
GO
