USE [oline]
GO
/****** Object:  Table [dbo].[os_cache_v2]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[os_cache_v2](
	[locationId] [int] NOT NULL,
	[entranceTypeCode] [varchar](10) NOT NULL,
	[personUuid] [varchar](100) NOT NULL,
	[personId] [datetime2](3) NOT NULL,
	[startTime] [datetime2](3) NOT NULL,
	[age] [smallint] NOT NULL,
	[gender] [smallint] NOT NULL,
	[genderScore] [int] NOT NULL,
	[visit] [smallint] NOT NULL,
	[match] [int] NOT NULL,
	[minMatchScore] [int] NOT NULL,
	[minMatchIntervalSec] [int] NOT NULL,
	[minMatchIntervalMajorSec] [int] NOT NULL,
	[buildTime] [datetime2](3) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
