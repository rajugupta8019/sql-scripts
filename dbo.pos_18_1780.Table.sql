USE [oline]
GO
/****** Object:  Table [dbo].[pos_18_1780]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pos_18_1780](
	[timestamp] [datetime] NOT NULL,
	[receiptNo] [varchar](100) NOT NULL,
	[netSales] [decimal](18, 2) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
