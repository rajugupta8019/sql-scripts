USE [oline]
GO
/****** Object:  Table [dbo].[oline_email_log]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oline_email_log](
	[timestamp] [datetime] NOT NULL,
	[templateCode] [nvarchar](20) NOT NULL,
	[data] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
