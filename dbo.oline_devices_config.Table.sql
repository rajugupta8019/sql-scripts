USE [oline]
GO
/****** Object:  Table [dbo].[oline_devices_config]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oline_devices_config](
	[MacAddress] [varchar](255) NOT NULL,
	[Property] [nvarchar](64) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[DateInfo] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
