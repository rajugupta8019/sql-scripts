USE [oline]
GO
/****** Object:  Table [dbo].[os_Trakomatic_OSense_Demo_Cam1]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_Trakomatic_OSense_Demo_Cam1](
	[personId] [datetime] NOT NULL,
	[fileName] [nvarchar](100) NOT NULL,
	[matchFileName] [nvarchar](100) NULL,
	[score] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_score]  DEFAULT ((0)),
	[isMerged] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_isMerged]  DEFAULT ((0)),
	[timestamp] [datetime] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_timestamp]  DEFAULT ((0)),
	[gender] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_gender]  DEFAULT ((0)),
	[age] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_age]  DEFAULT ((0)),
	[maleScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_maleScoreNormalized]  DEFAULT ((0)),
	[femaleScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_femaleScoreNormalized]  DEFAULT ((0)),
	[childScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_childScoreNormalized]  DEFAULT ((0)),
	[teenScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_teenScoreNormalized]  DEFAULT ((0)),
	[youngAdultScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_youngAdultScoreNormalized]  DEFAULT ((0)),
	[adultScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_adultScoreNormalized]  DEFAULT ((0)),
	[middleAgedScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_middleAgedScoreNormalized]  DEFAULT ((0)),
	[seniorScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_seniorScoreNormalized]  DEFAULT ((0)),
	[trackId] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_trackId]  DEFAULT ((0)),
	[normalizedGender] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_normalizedGender]  DEFAULT ((0)),
	[normalizedAge] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_normalizedAge]  DEFAULT ((0)),
	[normalizedAgeString] [nvarchar](50) NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_normalizedAgeString]  DEFAULT (N'N/A'),
	[maxThumbnailIndex] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_maxThumbnailIndex]  DEFAULT ((0)),
	[id] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_id]  DEFAULT ((0)),
	[maleScore] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_maleScore]  DEFAULT ((0)),
	[femaleScore] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_femaleScore]  DEFAULT ((0)),
	[enrolled] [smallint] NOT NULL CONSTRAINT [DF_os_Trakomatic_OSense_Demo_Cam1_enrolled]  DEFAULT ((0))
) ON [PRIMARY]

GO
