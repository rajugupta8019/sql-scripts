USE [oline]
GO
/****** Object:  StoredProcedure [dbo].[createOSenseThumbnailTable]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[createOSenseThumbnailTable]
(@tableName AS VARCHAR(100))
AS
BEGIN
	SELECT @tableName
	DECLARE @isExist INT
	SET @isExist = (SELECT COUNT(*) FROM sys.tables WHERE [name] = @tableName)
	
	IF @isExist <= 0
	BEGIN
		EXEC('CREATE TABLE [' + @tableName + ']('
		+ '[personId] DATETIME2(3) NOT NULL,'
		+ '[uuid] [varchar](100) NOT NULL,'
		+ '[thumbnail] [varbinary](max) NULL'
		+ ')')
	END
END

GO
