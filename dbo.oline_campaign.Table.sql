USE [oline]
GO
/****** Object:  Table [dbo].[oline_campaign]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oline_campaign](
	[campaignId] [uniqueidentifier] NOT NULL,
	[title] [nvarchar](100) NOT NULL,
	[startDate] [datetime2](3) NOT NULL,
	[endDate] [datetime2](3) NOT NULL,
	[locationId] [int] NOT NULL,
	[corporationId] [int] NOT NULL,
	[status] [smallint] NOT NULL,
	[detail] [varchar](max) NULL,
 CONSTRAINT [PK_oline_campaign] PRIMARY KEY CLUSTERED 
(
	[campaignId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
