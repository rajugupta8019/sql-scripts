USE [oline]
GO
/****** Object:  Table [dbo].[tr3_ACF2A264EDF38BAA3946D228B1E1B13C]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tr3_ACF2A264EDF38BAA3946D228B1E1B13C](
	[timestamp] [datetime2](3) NOT NULL,
	[points] [geometry] NOT NULL,
	[dwellTime] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
