USE [oline]
GO
/****** Object:  StoredProcedure [dbo].[usp_Import_Otrail_Demo]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Raju Gupta>
-- Description:	<Importing otrail to demo devices>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Import_Otrail_Demo]	
AS
BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		Declare @startdate date
		Declare @enddate date
		DECLARE @Random INT;
	    DECLARE @Upper INT;
	   DECLARE @Lower INT
	   declare @points geometry
	   declare @timestamp datetime2
	   declare @dwellTime int
  
	---- This will create a random number between 1 and 999
	SET @Lower = 1 ---- The lowest random number
	SET @Upper = 420 ---- The highest random number 
	--SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0)
	--SELECT @Random

	 select top 1  @startdate = CONVERT(char(10), timestamp,126) from tr3_AEA2CDB7EE1E4C0EE901B7074CE73ABD order by  CONVERT(char(10), timestamp,126) desc
	 set @enddate=dateadd(day,1,@startdate)

	 --print @startdate
	 --print @enddate

	-- insert into tr3_SMUTRA0112010T2050 
	--select  CONVERT(char(10), GetDate(),126) +' '+ convert(varchar(15), timestamp, 114),points,dwellTime+ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0) 
	--from tr3_AEA2CDB7EE1E4C0EE901B7074CE73ABD  where timestamp between @startdate and @enddate
	--select @counter=count(*) from tr3_AEA2CDB7EE1E4C0EE901B7074CE73ABD  where timestamp between @startdate and @enddate

	Declare cur_import cursor for select timestamp, points,dwellTime  from tr3_AEA2CDB7EE1E4C0EE901B7074CE73ABD  where timestamp between @startdate and @enddate

	open cur_import 
	Fetch next from cur_import into @timestamp, @points,@dwellTime
	while @@FETCH_STATUS = 0   
	begin

	SELECT @Random = ROUND(((@Upper - @Lower -1) * RAND() + @Lower), 0)
	insert into tr3_SMUTRA0112010T2050 values(CONVERT(char(10), GetDate(),126) +' '+ convert(varchar(15), @timestamp, 114),@points,@Random)
	Fetch next from cur_import into @timestamp, @points,@dwellTime
	end

	close cur_import
	deallocate cur_import

END





GO
