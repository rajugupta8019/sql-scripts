USE [oline]
GO
/****** Object:  Table [dbo].[oline_installer]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oline_installer](
	[installerId] [int] IDENTITY(1,1) NOT NULL,
	[data] [varchar](5000) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
