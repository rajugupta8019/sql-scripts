USE [oline]
GO
/****** Object:  Table [dbo].[os_OSENSEMACADDRESS_Cam3]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_OSENSEMACADDRESS_Cam3](
	[personId] [datetime] NOT NULL,
	[fileName] [nvarchar](100) NOT NULL,
	[matchFileName] [nvarchar](100) NULL,
	[score] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_score]  DEFAULT ((0)),
	[isMerged] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_isMerged]  DEFAULT ((0)),
	[timestamp] [datetime] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_timestamp]  DEFAULT ((0)),
	[gender] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_gender]  DEFAULT ((0)),
	[age] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_age]  DEFAULT ((0)),
	[maleScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_maleScoreNormalized]  DEFAULT ((0)),
	[femaleScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_femaleScoreNormalized]  DEFAULT ((0)),
	[childScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_childScoreNormalized]  DEFAULT ((0)),
	[teenScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_teenScoreNormalized]  DEFAULT ((0)),
	[youngAdultScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_youngAdultScoreNormalized]  DEFAULT ((0)),
	[adultScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_adultScoreNormalized]  DEFAULT ((0)),
	[middleAgedScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_middleAgedScoreNormalized]  DEFAULT ((0)),
	[seniorScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_seniorScoreNormalized]  DEFAULT ((0)),
	[trackId] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_trackId]  DEFAULT ((0)),
	[normalizedGender] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_normalizedGender]  DEFAULT ((0)),
	[normalizedAge] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_normalizedAge]  DEFAULT ((0)),
	[normalizedAgeString] [nvarchar](50) NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_normalizedAgeString]  DEFAULT (N'N/A'),
	[maxThumbnailIndex] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_maxThumbnailIndex]  DEFAULT ((0)),
	[id] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_id]  DEFAULT ((0)),
	[maleScore] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_maleScore]  DEFAULT ((0)),
	[femaleScore] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_femaleScore]  DEFAULT ((0)),
	[enrolled] [smallint] NOT NULL CONSTRAINT [DF_os_OSENSEMACADDRESS_Cam3_enrolled]  DEFAULT ((0))
) ON [PRIMARY]

GO
