USE [oline]
GO
/****** Object:  Table [dbo].[oline_devices_3]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oline_devices_3](
	[siteID] [bigint] IDENTITY(1,1) NOT NULL,
	[deviceID] [varchar](64) NOT NULL,
	[siteName] [nvarchar](128) NOT NULL,
	[groupID] [int] NULL,
	[meta_IP] [varchar](16) NULL,
	[meta_Lens] [varchar](8) NULL,
	[meta_Notes] [nvarchar](1024) NULL,
	[lastUpdateTime] [datetime] NULL,
	[latestVersion] [varchar](128) NULL,
	[ssTime] [int] NOT NULL,
	[color] [varchar](8) NOT NULL,
	[cacheLevel] [tinyint] NULL,
	[isGroup] [bit] NOT NULL,
	[maxVal] [int] NOT NULL,
	[tz] [varchar](8) NOT NULL,
	[updateInterval] [bigint] NULL,
	[isChild] [varchar](50) NULL,
	[StartTime] [char](10) NULL CONSTRAINT [DF_oline_devices_StartTime]  DEFAULT ('08:00'),
	[EndTime] [char](10) NULL CONSTRAINT [DF_oline_devices_EndTime]  DEFAULT ('21:30'),
 CONSTRAINT [PK_oline_devices_new_nv] PRIMARY KEY CLUSTERED 
(
	[siteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
