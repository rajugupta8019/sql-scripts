USE [oline]
GO
/****** Object:  Table [dbo].[OtrackSnapshot_config]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OtrackSnapshot_config](
	[dateInfo] [datetime2](3) NOT NULL,
	[SnapShotPath] [nvarchar](max) NULL,
	[deviceUuid] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
