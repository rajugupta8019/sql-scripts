USE [oline]
GO
/****** Object:  Table [dbo].[oline_sms_log]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oline_sms_log](
	[accountId] [int] NOT NULL,
	[contactNo] [varchar](20) NOT NULL,
	[timestamp] [datetime2](3) NOT NULL,
	[data] [nvarchar](max) NOT NULL,
	[smsType] [varchar](10) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
