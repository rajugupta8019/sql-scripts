USE [oline]
GO
/****** Object:  Table [dbo].[os_train]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[os_train](
	[userId] [varchar](100) NOT NULL,
	[personId] [datetime2](3) NOT NULL,
	[uuid] [varchar](100) NOT NULL,
	[gender] [smallint] NOT NULL,
	[age] [smallint] NOT NULL,
	[faceType] [varchar](10) NOT NULL,
	[trainedOn] [datetime2](3) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
