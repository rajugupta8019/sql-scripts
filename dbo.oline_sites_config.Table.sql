USE [oline]
GO
/****** Object:  Table [dbo].[oline_sites_config]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oline_sites_config](
	[siteID] [int] NOT NULL,
	[siteProperty] [nvarchar](50) NOT NULL,
	[siteValue] [nvarchar](256) NOT NULL,
	[DateInfo] [datetime] NOT NULL
) ON [PRIMARY]

GO
