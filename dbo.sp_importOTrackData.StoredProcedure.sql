USE [oline]
GO
/****** Object:  StoredProcedure [dbo].[sp_importOTrackData]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_importOTrackData]
@sourceUuid VARCHAR(100),
@destinationUUid VARCHAR(100),
@startTime VARCHAR(100),
@endTime VARCHAR(100)
AS
BEGIN
	DECLARE @query VARCHAR(MAX) = 'INSERT INTO [dbo].[oline_' + @destinationUuid + '_h] ([dateInfo],[entryCount],[exitCount])'
	+ ' (SELECT [dateInfo], [entryCount], [exitCount] FROM [dbo].[oline_' + @sourceUuid + '_h] WHERE [dateInfo] BETWEEN ''' + @startTime + ''' AND ''' + @endTime + ''')';
	print @query;
	exec(@query)
END

GO
