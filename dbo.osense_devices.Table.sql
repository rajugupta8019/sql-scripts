USE [oline]
GO
/****** Object:  Table [dbo].[osense_devices]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[osense_devices](
	[deviceID] [varchar](64) NOT NULL,
	[siteID] [bigint] NOT NULL,
	[siteName] [varchar](128) NOT NULL,
	[groupID] [int] NULL,
	[meta_IP] [varchar](50) NULL,
	[meta_Notes] [varchar](1024) NULL,
	[meta_Lens] [varchar](50) NULL,
	[lastUpdateTime] [datetime] NULL,
	[latestVersion] [varchar](128) NULL,
	[ssTime] [int] NOT NULL,
	[color] [varchar](8) NOT NULL,
	[cacheLevel] [tinyint] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
