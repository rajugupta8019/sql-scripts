USE [oline]
GO
/****** Object:  Table [dbo].[oline_devices]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oline_devices](
	[siteID] [bigint] IDENTITY(1,1) NOT NULL,
	[deviceID] [varchar](64) NOT NULL,
	[siteName] [nvarchar](128) NOT NULL CONSTRAINT [DF_oline_devices_siteName]  DEFAULT (N'New Device'),
	[groupID] [int] NULL CONSTRAINT [DF_oline_devices_groupID]  DEFAULT ((0)),
	[meta_IP] [varchar](16) NULL,
	[meta_Lens] [varchar](8) NULL,
	[meta_Notes] [nvarchar](1024) NULL,
	[lastUpdateTime] [datetime] NULL,
	[latestVersion] [varchar](128) NULL,
	[ssTime] [int] NOT NULL CONSTRAINT [DF_oline_devices_ssTime]  DEFAULT ((120)),
	[color] [varchar](8) NOT NULL CONSTRAINT [DF_oline_devices_color]  DEFAULT ('#4572A7'),
	[cacheLevel] [tinyint] NULL,
	[isGroup] [bit] NOT NULL CONSTRAINT [DF_oline_devices_isGroup]  DEFAULT ((0)),
	[maxVal] [int] NOT NULL CONSTRAINT [DF_oline_devices_maxVal]  DEFAULT ((500)),
	[tz] [varchar](8) NOT NULL CONSTRAINT [DF_oline_devices_tz]  DEFAULT ('+08:00'),
	[updateInterval] [bigint] NULL CONSTRAINT [DF_oline_devices_updateInterval]  DEFAULT ((30)),
	[isChild] [varchar](50) NULL CONSTRAINT [DF_oline_devices_isChild]  DEFAULT ('yes'),
	[StartTime] [varchar](10) NULL CONSTRAINT [DF_oline_devices_StartTime_1]  DEFAULT ('0700'),
	[CloseTime] [varchar](10) NULL CONSTRAINT [DF_oline_devices_CloseTime]  DEFAULT ('2300'),
	[Type] [varchar](10) NULL,
 CONSTRAINT [PK_oline_devices_new_2] PRIMARY KEY CLUSTERED 
(
	[siteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
