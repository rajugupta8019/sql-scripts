USE [oline]
GO
/****** Object:  Table [dbo].[os_643549588F87F0C8DCDF5FF2712BD6D4]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_643549588F87F0C8DCDF5FF2712BD6D4](
	[personId] [datetime2](3) NOT NULL,
	[fileName] [nvarchar](100) NOT NULL,
	[matchFileName] [nvarchar](100) NULL,
	[score] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_score]  DEFAULT ((0)),
	[isMerged] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_isMerged]  DEFAULT ((0)),
	[timestamp] [datetime2](3) NOT NULL,
	[gender] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_gender]  DEFAULT ((0)),
	[age] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_age]  DEFAULT ((0)),
	[maleScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_maleScoreNormalized]  DEFAULT ((0)),
	[femaleScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_femaleScoreNormalized]  DEFAULT ((0)),
	[childScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_childScoreNormalized]  DEFAULT ((0)),
	[teenScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_teenScoreNormalized]  DEFAULT ((0)),
	[youngAdultScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_youngAdultScoreNormalized]  DEFAULT ((0)),
	[adultScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_adultScoreNormalized]  DEFAULT ((0)),
	[middleAgedScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_middleAgedScoreNormalized]  DEFAULT ((0)),
	[seniorScoreNormalized] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_seniorScoreNormalized]  DEFAULT ((0)),
	[trackId] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_trackId]  DEFAULT ((0)),
	[normalizedGender] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_normalizedGender]  DEFAULT ((0)),
	[normalizedAge] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_normalizedAge]  DEFAULT ((0)),
	[normalizedAgeString] [nvarchar](50) NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_normalizedAgeString]  DEFAULT (N'N/A'),
	[maxThumbnailIndex] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_maxThumbnailIndex]  DEFAULT ((0)),
	[id] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_id]  DEFAULT ((0)),
	[maleScore] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_maleScore]  DEFAULT ((0)),
	[femaleScore] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_femaleScore]  DEFAULT ((0)),
	[enrolled] [smallint] NOT NULL CONSTRAINT [DF_os_643549588F87F0C8DCDF5FF2712BD6D4_enrolled]  DEFAULT ((0))
) ON [PRIMARY]

GO
