USE [oline]
GO
/****** Object:  Table [dbo].[tr_888603B121DF4884AA0DCD7E61F0BAAA]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tr_888603B121DF4884AA0DCD7E61F0BAAA](
	[trackId] [varchar](50) NOT NULL,
	[startTime] [datetime2](3) NOT NULL,
	[endTime] [datetime2](3) NOT NULL,
	[inTime] [int] NOT NULL,
	[frameData] [binary](3072) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
