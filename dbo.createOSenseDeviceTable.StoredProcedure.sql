USE [oline]
GO
/****** Object:  StoredProcedure [dbo].[createOSenseDeviceTable]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[createOSenseDeviceTable]
(@tableName AS VARCHAR(100))
AS
BEGIN
	SELECT @tableName
	DECLARE @isExist INT
	SET @isExist = (SELECT COUNT(*) FROM sys.tables WHERE [name] = @tableName)
	
	IF @isExist <= 0
	BEGIN
		EXEC('CREATE TABLE [' + @tableName + ']('
		+ '[personId] DATETIME2(3) NOT NULL,'
		+ '[fileName] [nvarchar](100) NOT NULL,'
		+ '[matchFileName] [nvarchar](100) NULL,'
		+ '[score] [smallint] NOT NULL,'
		+ '[isMerged] [smallint] NOT NULL,'
		+ '[timestamp] datetime2(3) NOT NULL,'
		+ '[gender] [smallint] NOT NULL,'
		+ '[age] [smallint] NOT NULL,'
		+ '[maleScoreNormalized] [smallint] NOT NULL,'
		+ '[femaleScoreNormalized] [smallint] NOT NULL,'
		+ '[childScoreNormalized] [smallint] NOT NULL,'
		+ '[teenScoreNormalized] [smallint] NOT NULL,'
		+ '[youngAdultScoreNormalized] [smallint] NOT NULL,'
		+ '[adultScoreNormalized] [smallint] NOT NULL,'
		+ '[middleAgedScoreNormalized] [smallint] NOT NULL,'
		+ '[seniorScoreNormalized] [smallint] NOT NULL,'
		+ '[trackId] [smallint] NOT NULL,'
		+ '[normalizedGender] [smallint] NOT NULL,'
		+ '[normalizedAge] [smallint] NOT NULL,'
		+ '[normalizedAgeString] [nvarchar](50) NOT NULL,'
		+ '[maxThumbnailIndex] [smallint] NOT NULL,'
		+ '[id] [smallint] NOT NULL,'
		+ '[maleScore] [smallint] NOT NULL,'
		+ '[femaleScore] [smallint] NOT NULL,'
		+ '[enrolled] [smallint] NOT NULL'
	+ ') ON [PRIMARY]')
	
		EXEC('ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_score]  DEFAULT ((0)) FOR [score]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_isMerged]  DEFAULT ((0)) FOR [isMerged]'
		--+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_timestamp]  DEFAULT ((0)) FOR [timestamp]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_gender]  DEFAULT ((0)) FOR [gender]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_age]  DEFAULT ((0)) FOR [age]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_maleScoreNormalized]  DEFAULT ((0)) FOR [maleScoreNormalized]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_femaleScoreNormalized]  DEFAULT ((0)) FOR [femaleScoreNormalized]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_childScoreNormalized]  DEFAULT ((0)) FOR [childScoreNormalized]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_teenScoreNormalized]  DEFAULT ((0)) FOR [teenScoreNormalized]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_youngAdultScoreNormalized]  DEFAULT ((0)) FOR [youngAdultScoreNormalized]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_adultScoreNormalized]  DEFAULT ((0)) FOR [adultScoreNormalized]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_middleAgedScoreNormalized]  DEFAULT ((0)) FOR [middleAgedScoreNormalized]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_seniorScoreNormalized]  DEFAULT ((0)) FOR [seniorScoreNormalized]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_trackId]  DEFAULT ((0)) FOR [trackId]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_normalizedGender]  DEFAULT ((0)) FOR [normalizedGender]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_normalizedAge]  DEFAULT ((0)) FOR [normalizedAge]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_normalizedAgeString]  DEFAULT (N''N/A'') FOR [normalizedAgeString]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_maxThumbnailIndex]  DEFAULT ((0)) FOR [maxThumbnailIndex]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_id]  DEFAULT ((0)) FOR [id]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_maleScore]  DEFAULT ((0)) FOR [maleScore]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_femaleScore]  DEFAULT ((0)) FOR [femaleScore]'
		+ ' ALTER TABLE [dbo].[' + @tableName + '] ADD  CONSTRAINT [DF_' + @tableName + '_enrolled]  DEFAULT ((0)) FOR [enrolled]'
		)
	
		SELECT 'Created'
	END
	ELSE
	BEGIN
		SELECT 'Existing'
	END
	
	--DECLARE @matchTableName NVARCHAR(100)
	--SET @matchTableName = @tableName + '_match'
	--EXEC [dbo].[createOSenseMatchTable] @matchTableName
END

GO
