USE [oline]
GO
/****** Object:  Table [dbo].[oline_device_group_bindings]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oline_device_group_bindings](
	[siteid_group] [int] NOT NULL,
	[siteid_device] [int] NOT NULL
) ON [PRIMARY]

GO
