USE [oline]
GO
/****** Object:  StoredProcedure [dbo].[createOtrailDataTable]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[createOtrailDataTable]
	@tableName AS VARCHAR(100),
	@table2Name AS VARCHAR(100)
AS
BEGIN
	SELECT @tableName
	DECLARE @isExist INT
	SET @isExist = (SELECT COUNT(*) FROM sys.tables WHERE [name] = @tableName)
	
	IF @isExist <= 0
	BEGIN
		EXEC('CREATE TABLE [' + @tableName + ']('
		+ '[trackId] [varchar](50) NOT NULL,'
		+ '[startTime] [datetime2](3) NOT NULL,'
		+ '[endTime] [datetime2](3) NOT NULL,'
		+ '[inTime] [int] NOT NULL,'
		+ '[frameData] [binary](3072) NOT NULL'
		+ ')')
	END
	/*
	SET @isExist = (SELECT COUNT(*) FROM sys.tables WHERE [name] = @table2Name)
	IF @isExist <= 0
	BEGIN
		EXEC('CREATE TABLE [' + @table2Name + ']('
		+ '[trackId] [varchar](50) NOT NULL,'
		+ '[startTime] [datetime2](3) NOT NULL,'
		+ '[x] [numeric] (18,3) NOT NULL,'
		+ '[y] [numeric](18,3) NOT NULL'
		+ ')')
	END
	*/
END

GO
