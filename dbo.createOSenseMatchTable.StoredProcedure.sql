USE [oline]
GO
/****** Object:  StoredProcedure [dbo].[createOSenseMatchTable]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[createOSenseMatchTable]
(@tableName AS VARCHAR(100))
AS
BEGIN
	SELECT @tableName
	DECLARE @isExist INT
	SET @isExist = (SELECT COUNT(*) FROM sys.tables WHERE [name] = @tableName)
	
	IF @isExist <= 0
	BEGIN
		EXEC('CREATE TABLE [' + @tableName + ']('
		+ '[personId] DATETIME2(3) NOT NULL,'
		+ '[matchPersonId] DATETIME2(3) NOT NULL,'
		+ '[matchScore] [SMALLINT]NOT NULL,'
		+ '[matchType] [TINYINT]NOT NULL'
		+ ')')
	END
END
GO
