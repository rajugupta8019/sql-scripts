USE [oline]
GO
/****** Object:  Table [dbo].[oline_software_config]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oline_software_config](
	[originalFileName] [varchar](255) NOT NULL,
	[property] [varchar](100) NOT NULL,
	[value] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
