USE [oline]
GO
/****** Object:  Table [dbo].[oline_system_config]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oline_system_config](
	[key] [nvarchar](64) NOT NULL,
	[value] [nvarchar](256) NULL
) ON [PRIMARY]

GO
