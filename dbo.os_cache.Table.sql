USE [oline]
GO
/****** Object:  Table [dbo].[os_cache]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[os_cache](
	[timestamp] [datetime] NOT NULL,
	[galleryId] [varchar](100) NOT NULL,
	[detail] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
