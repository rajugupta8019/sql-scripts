USE [oline]
GO
/****** Object:  Table [dbo].[tr3_2363EBD5AB6987B6128300F768D7C79F]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tr3_2363EBD5AB6987B6128300F768D7C79F](
	[timestamp] [datetime2](3) NOT NULL,
	[points] [geometry] NOT NULL,
	[dwellTime] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
