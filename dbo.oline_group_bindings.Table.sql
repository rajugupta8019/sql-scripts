USE [oline]
GO
/****** Object:  Table [dbo].[oline_group_bindings]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oline_group_bindings](
	[group_siteid] [int] NULL,
	[device_siteid] [int] NOT NULL
) ON [PRIMARY]

GO
