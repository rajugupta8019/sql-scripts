USE [oline]
GO
/****** Object:  Table [dbo].[tr3_AEA2CDB7EE1E4C0EE901B7074CE73ABD]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tr3_AEA2CDB7EE1E4C0EE901B7074CE73ABD](
	[timestamp] [datetime2](3) NOT NULL,
	[points] [geometry] NOT NULL,
	[dwellTime] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
