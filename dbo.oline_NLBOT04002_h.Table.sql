USE [oline]
GO
/****** Object:  Table [dbo].[oline_NLBOT04002_h]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oline_NLBOT04002_h](
	[dateInfo] [datetime2](3) NOT NULL,
	[entryCount] [int] NULL,
	[exitCount] [int] NULL
) ON [PRIMARY]

GO
