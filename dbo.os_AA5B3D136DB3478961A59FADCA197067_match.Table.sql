USE [oline]
GO
/****** Object:  Table [dbo].[os_AA5B3D136DB3478961A59FADCA197067_match]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_AA5B3D136DB3478961A59FADCA197067_match](
	[personId] [datetime2](3) NOT NULL,
	[matchPersonId] [datetime2](3) NOT NULL,
	[matchScore] [smallint] NOT NULL,
	[matchType] [tinyint] NOT NULL
) ON [PRIMARY]

GO
