USE [oline]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetotrackSnapshotPath]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Raju Gupta>
-- Description:	<selecting account for report>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetotrackSnapshotPath]	
@Datetimestamp Datetime,@deviceuuid nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;  
	Declare @startdatetime datetime2
		Declare @enddatetime datetime2 


set @startdatetime=DATEADD(minute,-5,@Datetimestamp)

set @enddatetime=DATEADD(minute,5,@Datetimestamp)

	select top 1 * from OtrackSnapshot_config where dateInfo between @Datetimestamp and @enddatetime  and deviceUuid=@deviceuuid order by dateInfo desc


END
GO
