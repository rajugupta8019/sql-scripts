USE [oline]
GO
/****** Object:  Table [dbo].[os_testing_Cam1]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_testing_Cam1](
	[personId] [nvarchar](50) NOT NULL,
	[fileName] [nvarchar](100) NOT NULL,
	[matchFileName] [nvarchar](100) NULL,
	[score] [smallint] NOT NULL,
	[isMerged] [smallint] NOT NULL,
	[timestamp] [datetime] NOT NULL,
	[gender] [smallint] NOT NULL,
	[age] [smallint] NOT NULL,
	[maleScoreNormalized] [smallint] NOT NULL,
	[femaleScoreNormalized] [smallint] NOT NULL,
	[childScoreNormalized] [smallint] NOT NULL,
	[teenScoreNormalized] [smallint] NOT NULL,
	[youngAdultScoreNormalized] [smallint] NOT NULL,
	[adultScoreNormalized] [smallint] NOT NULL,
	[middleAgedScoreNormalized] [smallint] NOT NULL,
	[seniorScoreNormalized] [smallint] NOT NULL,
	[trackId] [smallint] NOT NULL,
	[normalizedGender] [smallint] NOT NULL,
	[normalizedAge] [smallint] NOT NULL,
	[normalizedAgeString] [nvarchar](50) NOT NULL,
	[maxThumbnailIndex] [smallint] NOT NULL,
	[id] [smallint] NOT NULL,
	[maleScore] [smallint] NOT NULL,
	[femaleScore] [smallint] NOT NULL,
	[enrolled] [smallint] NOT NULL
) ON [PRIMARY]

GO
