USE [oline]
GO
/****** Object:  Table [dbo].[oline_control_center_ping]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[oline_control_center_ping](
	[id] [varchar](100) NOT NULL,
	[data] [nvarchar](max) NOT NULL,
	[timestamp] [datetime] NOT NULL,
	[groupId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
