USE [oline]
GO
/****** Object:  StoredProcedure [dbo].[OTrackAddEntryExitData]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[OTrackAddEntryExitData]
(
@macaddress varchar (50),
@datestr varchar (50),
@columnname varchar(50),
@value int)
AS

declare @tablename varchar (50)
set @tablename = 'oline_' + @macaddress + '_h'

exec ('select dateinfo from ' + @tablename + ' where dateinfo = CONVERT(datetime, ''' + @datestr + ''')')

if @@RowCount != 0
begin
	select 'updating'
	exec('update ' + @tablename + ' set ' + @columnname + ' = ' + @value + ' where dateInfo = CONVERT(datetime, ''' + @datestr + ''')')
	exec ('select * from ' + @tablename + ' WHERE dateInfo =  CONVERT(datetime, ''' + @datestr + ''')')
end


else
begin
	select 'inserting'
	exec('insert into ' + @tablename + ' (dateInfo, ' + @columnname + ') values (' + 'CONVERT(datetime, ''' + @datestr + ''')' + ',' + @value + ')')	
	exec ('select * from ' + @tablename + ' WHERE dateInfo = CONVERT(datetime, ''' + @datestr + ''')')
end
GO
