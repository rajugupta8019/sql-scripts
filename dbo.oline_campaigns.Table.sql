USE [oline]
GO
/****** Object:  Table [dbo].[oline_campaigns]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oline_campaigns](
	[id] [int] NOT NULL,
	[gid] [int] NULL,
	[uid] [int] NULL,
	[siteid] [int] NULL,
	[startdate] [date] NULL,
	[enddate] [date] NULL,
	[goal] [bigint] NULL,
	[title] [nvarchar](50) NULL,
	[manager] [nvarchar](50) NULL
) ON [PRIMARY]

GO
