USE [oline]
GO
/****** Object:  Table [dbo].[tr2_217801164E28D4DCC2C425AD07FD911E]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tr2_217801164E28D4DCC2C425AD07FD911E](
	[trackId] [varchar](50) NOT NULL,
	[startTime] [datetime2](3) NOT NULL,
	[x] [numeric](18, 3) NOT NULL,
	[y] [numeric](18, 3) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
