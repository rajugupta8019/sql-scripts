USE [oline]
GO
/****** Object:  StoredProcedure [dbo].[AddOTrackDevice]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create  PROCEDURE [dbo].[AddOTrackDevice]
(
@macaddress varchar(50),
@type varchar(10))
AS

declare @tablename varchar (50)

set @tablename = 'oline_' + @macaddress + '_h'

if exists(select 1 from INFORMATION_SCHEMA.TABLES T where T.TABLE_NAME = @tablename) 
begin
	select 'Table Exists'
end
else
	begin
	select 'Table dne. Creating...'

	declare @constraintname varchar (50)
	declare @alphabet varchar(36) = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
	declare @randomstring varchar(10)

	declare @SQL varchar(1024)
	declare @CreateTableSQL varchar (1024)
	
	set @randomstring = substring(@alphabet, convert(int, rand()*36), 1) + substring(@alphabet, convert(int, rand()*36), 1) + substring(@alphabet, convert(int, rand()*36), 1) + substring(@alphabet, convert(int, rand()*36), 1) + substring(@alphabet, convert(int, rand()*36), 1);
	set @constraintname = 'PK_oline_' + @macaddress + '_h_' + @randomstring

	set @CreateTableSQL = 'CREATE TABLE [dbo].[' + @tablename + ']([dateInfo] [datetime] NOT NULL, [entryCount] [int] NULL, [exitCount] [int] NULL, CONSTRAINT [' + @constraintname + '] PRIMARY KEY CLUSTERED ([dateInfo] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]' 

	exec(@CreateTableSQL)
	exec('SELECT * From ' + @tablename)

end

if EXISTS (select 1 from oline_devices WHERE deviceid = @macaddress)
begin
	select 'macaddress exists in oline_devices'    
end

else
begin
	declare @InsertIntoSQL varchar (1024)
	set @InsertIntoSQL = 'INSERT INTO oline_devices(deviceID, siteName, groupID, type) values(''' + @macaddress + ''', ''' + @macaddress + ''', 1, ''' + @type + ''')'
	select 'macaddress dne in oline_devices. inserting...'
	exec(@InsertIntoSQL)
	exec('SELECT * From oline_devices')
   -- INSERT HERE
end
GO
