USE [oline]
GO
/****** Object:  Table [dbo].[os_734202B04866E644DFA1141CCE52CF87_match]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[os_734202B04866E644DFA1141CCE52CF87_match](
	[personId] [datetime2](3) NOT NULL,
	[matchPersonId] [datetime2](3) NOT NULL,
	[matchScore] [smallint] NOT NULL,
	[matchType] [tinyint] NOT NULL
) ON [PRIMARY]

GO
