USE [oline]
GO
/****** Object:  Table [dbo].[tn_26-1855-20140811164418866]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tn_26-1855-20140811164418866](
	[personId] [datetime2](3) NOT NULL,
	[uuid] [varchar](100) NOT NULL,
	[thumbnail] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
