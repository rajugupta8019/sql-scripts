USE [oline]
GO
/****** Object:  Table [dbo].[tr3_ACF94D5682A907528DA5733B5D233603]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tr3_ACF94D5682A907528DA5733B5D233603](
	[timestamp] [datetime2](3) NOT NULL,
	[points] [geometry] NOT NULL,
	[dwellTime] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
