USE [oline]
GO
/****** Object:  Table [dbo].[tr_994CF6017090163CA36412C46CE0F16C]    Script Date: 12/30/2015 3:55:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tr_994CF6017090163CA36412C46CE0F16C](
	[trackId] [varchar](50) NOT NULL,
	[startTime] [datetime2](3) NOT NULL,
	[endTime] [datetime2](3) NOT NULL,
	[inTime] [int] NOT NULL,
	[frameData] [binary](3072) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
